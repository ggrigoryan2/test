<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tasks', 'CommentController@index');
Route::post('/task', 'CommentController@store');
Route::delete('/task/{task}', 'CommentController@destroy');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/*
 Route::post('task', 'AddController@insert');
 Route::get('task', function()
 {
     $tasks = Task::all();
 
     return View::make('tasks')->with('tasks', $utasks);
 });
 */