@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                   <form action="{{ url('task') }}" method="POST" class="form-horizontal">
      {{ csrf_field() }}

      <!-- Имя задачи -->
      <div class="form-group">
        <label for="task" class="col-sm-3 control-label">Заметка</label>

        <div class="col-sm-6">
          <input type="text" name="name" id="task-name" class="form-control">
        </div>
      </div>

      <!-- Кнопка добавления задачи -->
      <div class="form-group">
        <div class="col-sm-offset-3 col-sm-6">
          <button type="submit" class="btn btn-default">
            <i class="fa fa-plus"></i> Добавить заметку
          </button>
        </div>
      </div>
    </form>
    <div class="panel panel-default">
      <div class="panel-heading">
        Текущие зметки
      </div>
      <div class="panel-body">
        <table class="table table-striped task-table">

          <!-- Заголовок таблицы -->
          <thead>
            <th>Зметкa</th>
            <th>&nbsp;</th>
          </thead>

          <!-- Тело таблицы -->
          <tbody>
    @foreach($tasks as $task)
          <tr>
                <!-- Имя задачи -->
                <td class="table-text">
                  <div>{{$task->name}}</div>
                   </td>

                <td>

             <form action="{{ url('task/'.$task->id) }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}

            <button type="submit" id="delete-task-{{ $task->id }}" class="btn btn-danger">
                <i class="fa fa-btn fa-trash"></i>Удалить зметку
            </button>
        </form>
         
                </td>
              </tr>
      @endforeach
                   </tbody>
        </table>
      </div>
    </div>
</tbody>
        </table>
      </div>
    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
