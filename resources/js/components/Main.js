import React, { Component } from 'react';
import ReactDOM from 'react-dom';
 
/* Main Component */
class Main extends Component {
    constructor() {
   
        super();
     
    
        this.state = {
            products: [],
            currentProduct: null
        }
      }
      
      componentDidMount() {
        //code omitted for brevity
      }

      renderProducts() {
    return this.state.products.map(product => {
        return (
            //this.handleClick() method is invoked onClick.
            <li onClick={
                () =>this.handleClick(product)} key={product.id} >
                { product.title } 
            </li>      
        );
    })
  }
   
   handleClick(product) {
    //handleClick is used to set the state
    this.setState({currentProduct:product});
   
  }
   
  render() {
   /* Some css code has been removed for brevity */
    return (
        <div class="content">
                <div class="title m-b-md">
                Payzmart
                </div>

                <div class="links">
                    <a href="/CreateOrder">Create Order and Authorize Payment</a>
                    <a href="/FinalizeOrder">Finalize Authorization</a>
                    <a href="/UpdateOrder">Update Order Line Items</a>
                    <a href="/UpdateMerchantReference">Update Merchant Reference</a>
                    <a href="/soap">SOAP documentation</a>
                </div>
                
            </div>
       
    );
  }
}
 
export default Main;
 
/* The if statement is required so as to Render the component on pages that have a div with an ID of "root";  
*/
 
if (document.getElementById('root')) {
    ReactDOM.render(<Main />, document.getElementById('root'));
}


/*
class Main extends Component {
 

 
 renderProducts() {
    return this.state.products.map(product => {
        return (
            //this.handleClick() method is invoked onClick.
            <li onClick={
                () =>this.handleClick(product)} key={product.id} >
                { product.title } 
            </li>      
        );
    })
  }
   
   handleClick(product) {
    //handleClick is used to set the state
    this.setState({currentProduct:product});
   
  }
   
  render() {

    return (
        <div>
              <ul>
                { this.renderProducts() }
              </ul> 
            </div> 
       
    );
  }
}
 
export default Main;
 

 
if (document.getElementById('root')) {
    ReactDOM.render(<Main />, document.getElementById('root'));
}
*/
