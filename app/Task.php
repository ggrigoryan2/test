<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*
$user = App\User::find(1);

foreach ($user->tasks as $task) {
    echo $task->name;
}
*/

class Task extends Model
{
    protected $fillable = ['name'];

   public function user()
    {
        return $this->belongsTo(User::class);
    } 

}
