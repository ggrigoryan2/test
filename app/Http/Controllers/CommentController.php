<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class CommentController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       // $tasks = Task::all();
       // dump($tasks);
     /*   $tasks = Task::select(['id','name'])->get();
        return view('tasks')->with([
            'tasks' => $tasks
        ]);

        */
        $tasks = $request->user()->tasks()->get();
    return view('tasks', [
        'tasks' => $tasks,
    ]);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:600',
        ]);

       /*     $data = $request->all();
            $task = new Task;
            $task->fill($data);
            $task->save();
        
*/
        $request->user()->tasks()->create([
            'name' => $request->name,
        ]);
    

        return redirect('/tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Task $task)
    {
       // $this->authorize('destroy', $task);

        $task->delete();

        return redirect('/tasks');
    }
}
