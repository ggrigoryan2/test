<?php
    include 'core/init.php';

    if($user->is_loggedin()){
        redirect('index.php');
        exit();
    }

    if($detect->isMobile()){
        redirect('http://m.website.com/prijava.php');
        exit();
    }

    if(isset($_POST['prijava'])){
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if(time() - $user->failTime($post['email']) < 600 && $user->failCount($post['email']) >= 3){
            $err = '<p style="color:red;">Račun vam je zaključan na 10 minuta.</p>';
        }else{          
            if(empty($post['email']) || empty($post['password'])){
                $err = '';
            }elseif($id = $user->prijava($post['email'], $post['password'])){
                if($user->isActive($post['email'])){                    
                    $_SESSION['user'] = $id['id'];
                    $user->resetFail();
                    redirect('index.php');
                    exit();
                }else{
                    $err = '<p style="color:red;">Korisnički račun nije aktiviran.</p>';    
                }           
            }else{
                if($user->checkMail($post['email'])){
                    if($user->storeFail($post['email'])){
                        $err = '<p style="color:red;">Lozinka i email se ne poklapaju.</p>';
                    }
                }else{
                    $err = '<p style="color:red;">Lozinka i email se ne poklapaju.</p>';
                }           
            }
        }       
    }
?>